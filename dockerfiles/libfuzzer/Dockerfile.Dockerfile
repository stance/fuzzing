## Libfuzzer dockerfile
FROM ubuntu:16.04


ENV LANG C.UTF-8

RUN apt-get update &&\
    apt-get install -f -y apt-utils &&\
    apt-get -f -y upgrade &&\
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -f -y \ 
            wget \
            gnupg2 \
            git \
            python-dev

# Setup llvm repo
RUN ["/bin/bash", "-c", "set -o pipefail && wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -"]
RUN echo 'deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic main\n\
deb-src http://apt.llvm.org/bionic/ llvm-toolchain-bionic main\n'\
>> /etc/apt/sources.list

# Install llvm nightly
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update &&\
    apt-get install -f -y \
            llvm-8 \
            llvm-8-dev \
            llvm-8-doc \
            llvm-8-examples \
            llvm-8-runtime \
            clang-8 \
            clang-tools-8 \
            clang-8-doc \
            libclang-common-8-dev \
            libclang-8-dev \
            libclang1-8 \
            clang-format-8 \
            python-clang-8 \
            libfuzzer-8-dev \
            lldb-8 \
            lld-8 \
            libc++-8-dev \
            libc++-8-dev \
            libomp-8-dev &&\
# Clean up all temporary files
    apt-get clean &&\
    apt-get autoclean -y &&\
    apt-get autoremove -y &&\
    apt-get clean &&\
    rm -rf /tmp/* /var/tmp/* &&\
    rm -rf /var/lib/apt/lists/* &&\
    rm -f /etc/ssh/ssh_host_*

# compile target program

# To run the fuzzer, pass zero or more corpus directories as command line arguments.
# http://llvm.org/docs/LibFuzzer.html
ENTRYPOINT ./fuzzer [-flag1=val1 [-flag2=val2 ...] ] [dir1 [dir2 ...] ]